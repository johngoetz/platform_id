import pathlib
import platform


def read_os_release_file(release_file):
    release_file = pathlib.Path(release_file)
    if release_file.is_file():
        lines = filter(None, map(str.strip, release_file.read_text().split('\n')))
        return {k: v.strip('\'"') for k, v in map(lambda line: line.split('='), lines)}


def get_linux_info():
    info = read_os_release_file('/etc/os-release')
    if not info:
        try:
            dist = Popen(['lsb_release', '-si']).communicate()[0].strip()
            ver = Popen(['lsb_release', '-sr']).communicate()[0].strip()
            info = {'ID': dist, 'VERSION_ID': ver}
        except:
            pass
    if not info:
        info = read_os_release_file('/etc/lsb-release')
        if info:
            info['ID'] = info['DISTRIB_ID']
            info['VERSION_ID'] = info['DISTRIB_RELEASE']
    if not info:
        info = read_os_release_file('/etc/redhat-release')
    if not info:
        info = read_os_release_file('/etc/SuSe-release')
    if not info:
        dist, ver = platform.linux_distribution()[:2]
        info = {'ID': dist, 'VERSION_ID': ver}
    dist = info['ID'].split('-')[0].lower()
    ver = '.'.join(info['VERSION_ID'].split('.')[:2])
    return dist, ver


def platform_id():
    system = platform.system()
    if system == 'Linux':
        dist, ver = get_linux_info()
        return f'{dist}{ver}'
    elif system == 'Darwin':
        dist = 'macos'
        ver = '.'.join(platform.mac_ver()[0].split('.')[:2])
        machine = platform.machine()
        return f'{dist}{ver}-{machine}'
    elif system == 'Windows':
        dist = 'windows'
        ver = platform.win32_ver().split('.')[0]
        return f'{dist}{ver}'
    else:
        raise ValueError(f'unknown system: {system}')


if __name__ == '__main__':
    print(platform_id())
