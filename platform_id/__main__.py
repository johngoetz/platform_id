import sys

import platform_id


def main():
    if '--version' in sys.argv:
        print(platform_id.__version__)
    else:
        print(platform_id.platform_id())


if __name__ == '__main__':
    sys.exit(main() or 0)
