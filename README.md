# platform_id

Python module and command-line executable to print out a platform/system identification string to
be used for organizing files and branches.

## Installation and Setup

Use pip to install:
```shell
python -mpip install --user --upgrade git+https://gitlab.com/johngoetz/platform_id.git
```

For all systems and specifically macOS, make sure you have python's scripts directory in your
`PATH` environment variable:
```shell
>which platform-id
~/Library/Python/<majver>.<minver>/bin/platform-id
```
where `<majver>.<minver>` is the major and minor Python version where pip
installed the notary application.

## Command-Line Usage

The setup script will install a console script `platform-id` that prints out the string:
```shell
> platform-id
centos7
```

## Python Module Usage

Once installed, the `platform_id` module can be imported. The library contains only a single
method: `platform_id.platform_id()`. This will print the platform id to the console:
```python
import platform_id

pid = platform_id.platform_id()
print(pid)
```
