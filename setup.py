import pathlib
import re

from setuptools import setup


def read_version():
    here = pathlib.Path(__file__).parent.resolve()
    versionfile = here/'platform_id/version.py'
    m = re.search(r"__version__ = '(.*?)'", versionfile.read_text(), re.M)
    return m.group(1)


def setup_opts():
    return dict(
        name='platform-id',
        version=read_version(),
        description='Returns a platform/system identifaction string.',
        author='John T. Goetz',
        author_email='j.goetz@tecplot.com',
        classifiers=[
            # Development Status
            #   1 - Planning
            #   2 - Pre-Alpha
            #   3 - Alpha
            #   4 - Beta
            #   5 - Production/Stable
            #   6 - Mature
            #   7 - Inactive
            'Development Status :: 5 - Production/Stable',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Natural Language :: English',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3',
            'Topic :: Utilities',
        ],
        keywords=[],
        install_requires=[],
        packages=['platform_id'],
        entry_points={"console_scripts": ["platform-id=platform_id.__main__:main"]}
    )


if __name__ == '__main__':
    setup(**setup_opts())
