import sys
import unittest

import platform_id


class TestVersion(unittest.TestCase):
    def test_version(self):
        self.assertRegex(platform_id.__version__, '\d+\.\d+\.\d+')


if __name__ == '__main__':
    sys.exit(unittest.main())
