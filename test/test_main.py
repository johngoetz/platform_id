import contextlib
import io
import sys
import unittest
from unittest.mock import patch

import platform_id.__main__


class TestMain(unittest.TestCase):
    def test_main(self):
        with io.StringIO() as out, io.StringIO() as err:
            with contextlib.redirect_stdout(out), contextlib.redirect_stderr(err):
                with patch.object(sys, 'argv', ['platform_id']):
                    self.assertIsNone(platform_id.__main__.main())

            valid_platforms = ('windows', 'macos', 'fedora', 'ubuntu', 'opensuse', 'centos', 'redhat')
            self.assertTrue(any(x in out.getvalue() for x in valid_platforms))


if __name__ == '__main__':
    sys.exit(unittest.main())
