import sys
import unittest

import platform_id


class TestPlatformID(unittest.TestCase):
    def test_platform_id(self):
        pid = platform_id.platform_id()
        valid_platforms = ('windows', 'macos', 'fedora', 'ubuntu', 'opensuse', 'centos', 'redhat')
        self.assertTrue(any(x in pid for x in valid_platforms))


if __name__ == '__main__':
    sys.exit(unittest.main())
