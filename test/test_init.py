import sys
import unittest

import platform_id


class TestInit(unittest.TestCase):
    def test_imports(self):
        imports = set(dir(platform_id))
        expected = set(['platform_id', 'version'])
        self.assertTrue(imports.issuperset(expected))


if __name__ == '__main__':
    sys.exit(unittest.main())
