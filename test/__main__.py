import unittest
import sys

from test.test_init import *
from test.test_main import *
from test.test_platform_id import *
from test.test_version import *


if __name__ == '__main__':
    sys.exit(unittest.main())
